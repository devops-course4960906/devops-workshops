
build a Helm chart for installing Nginx with multiple versions, you can follow these steps:

1.  Set up a Helm chart structure:  
    Create a new directory for your Helm chart and navigate into it.
    
```
helm create nginx-chart
```

    
2.  Create the chart metadata:  
    Create a file named  `Chart.yaml`  and add the following content:
    

    ```
    apiVersion: v2
    name: nginx-chart
    appVersion: 0.1.21
    ```
    
    
3.  Create the default values:  
    Create a file named  `values.yaml`  and add the following content:

    
    ```
    nginxVersion: "1.21.3"
    ```
        
4.  Create the Nginx deployment template:  
    Inside the  `templates`  directory, create a file named  `nginx-deployment.yaml`  and add the following content:
    

    
    ```
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: nginx-deployment
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: nginx
      template:
        metadata:
          labels:
            app: nginx
        spec:
          containers:
            - name: nginx
              image: nginx:{{ .Values.nginxVersion }}
              ports:
                - containerPort: 80
    ```
    
    This template defines a Kubernetes Deployment that will run an Nginx container.


5.  Update the values:  
    Replace the content of the  `values.yaml`  file with the following:
    
    
    ```
    nginxVersion: "1.21.3"
    ```
    
    
6.  Install the chart:  
    Run the following command to install the chart:
    
    bash
    
    Copy
    
    ```
    helm install my-nginx ./nginx-chart
    ```

    
7.  Upgrade the chart with a different version:  
    To install a different version of Nginx, you can upgrade the chart using the following command:
    
    
    ```
    helm upgrade my-nginx ./nginx-chart --set nginxVersion="1.22.0"

    ```
    

By following these steps, you can create a Helm chart for installing Nginx with multiple versions and easily manage the deployments using Helm commands

8. Package nginx helm chart

```
helm package nginx-chart
```

9. Login to helm registry OCI://. 


```
helm registry login registry-1.docker.io -u omarabdalhamid
```
10. Push helm nginx-chart to OCI registry 

```
 helm push nginx-0.1.2.tgz  oci://registry-1.docker.io/omarabdalhamid
```

11. upgrade version of helm

```
helm upgrade webserver oci://registry-1.docker.io/omarabdalhamid/webserver --version 0.1.3
```


12. upgrade version of vaules inside chart

```
helm upgrade webserver oci://registry-1.docker.io/omarabdalhamid/webserver --set nginxVersion="1.22.0"
```