pipeline {

 // ####################### Jenkins Kubernetes Agent ########################   
  agent {
    kubernetes {
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: bash
            image: bash
            command:
            - cat
            tty: true
          - name: sonarqube
            image: sonarsource/sonar-scanner-cli
            command:
            - cat
            tty: true            
          - name: composer
            image: composer:latest
            command:
            - cat
            tty: true
          - name: node14
            image: node:14-alpine
            command:
            - cat
            tty: true
          - name: node
            image: node:16-alpine3.12
            command:
            - cat
            tty: true    
          - name: dind
            image: earthly/dind
            command:
            - cat
            tty: true      
          - name: kaniko
            image: gcr.io/kaniko-project/executor:v1.14.0-debug
            command:
                - sleep
            args:
                - 9999999    
            tty: true     
        '''
    }
  }

// ####################### CI Stages  ########################   


  stages {

 // ####################### Clone project  ########################   
            stage('Clone_Project') {
                steps {
                    container('bash') {
                    sh "apk add git "
                    sh "rm -rf ./*  && git clone https://gitlab.com/devops-course4960906/laravel-docker-lab.git"
                    }

                }  
            }


// ####################### Sonarqube testing   Stage  ########################            

            stage('Sonarqube_Project') {
                steps { 
                    container('sonarqube') { 
                       sh "cd laravel-docker-lab/src/ && sonar-scanner  -Dsonar.projectKey=laravelapp  -Dsonar.sources=.  -Dsonar.host.url=http://ec2-35-172-191-127.compute-1.amazonaws.com:31090  -Dsonar.login=sqp_312d4d08fd70f5486e6c2c7af37d8a46a84290d3"
                    }
                } 

            }


// ####################### Trivy Scanner testing   Stage  ######################## 


        stage('Trivy_Scanner') {
            steps {
                container('sonarqube') {
                sh "apk add curl"
                sh 'curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.18.3'
                sh 'curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/html.tpl > html.tpl'
                sh "curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b /usr/local/bin"

                sh 'trivy image --format template --template \"@./html.tpl\" --output report.html  node:11-alpine'
                sh "ls -lha "
                sh  "cat report.html "
                }

            }
           post {
                always {
                    archiveArtifacts artifacts: "report.html", fingerprint: true
                        
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: '.',
                        reportFiles: 'report.html',
                        reportName: 'Trivy Scan',
                        ])
                    }
        } 
        }
// ####################### Build Laravel APP Stage  ######################## 


            stage('Build_Laravel_APP') {
                steps { 
                    container('composer') {
                    sh "cd laravel-docker-lab/src && composer update "
                    sh "cd laravel-docker-lab/src && ls -lha ./vendor"
                    sh "cd laravel-docker-lab/src && ls -lha ./vendor/bin"
                    }
                }  
            }    

// ####################### Php Unit testing APP Stage  ######################## 


            stage('Php_Unit_Testing') {
                steps { 
                    container('composer') {     
                    // sh "cd laravel-docker-lab/src &&  composer require --dev phpunit/phpunit ^10 "
                    sh "cd laravel-docker-lab/src &&  ./vendor/bin/phpunit tests"
                    }
                }  
            }    

// ####################### Postman API testing   Stage  ######################## 

	stage('Postman API Testing ') {

           steps {
            container('node') {             
              sh "npm install -g newman"
              sh "npm install -g newman-reporter-htmlextra"
              sh 'newman run https://gitlab.com/vdespa/postman-tests/-/raw/master/collection.json --reporters cli,junit,htmlextra --reporter-junit-export "newman_result.xml" --reporter-htmlextra-export "newman_result.html" '
              junit "*.xml"
            }  
             
           }

            post {
                always {
                    archiveArtifacts artifacts: "newman_result.html", fingerprint: true
                        
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: '.',
                        reportFiles: 'newman_result.html',
                        reportName: 'NewMan Scan',
                        ])
                    }
        } 
           
    }


// ####################### Publish to ECR  ######################## 


            stage('Publish ECR') {
                steps { 
                    container('bash') {     
                      withEnv(["AWS_ACCESS_KEY_ID=${env.AWS_ACCESS_KEY_ID}", "AWS_SECRET_ACCESS_KEY_ID=${env.AWS_SECRET_ACCESS_KEY_ID}", "AWS_REGION=${env.AWS_REGION_ID}" ]){
                           sh "apk add docker"
                           sh "apk add aws-cli"
                           //sh "echo $ECR_PASSWORD > pass.txt"
                           sh """
                                #!/bin/bash
                                      export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
                                      export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY_ID
                                      aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 452226382718.dkr.ecr.us-east-1.amazonaws.com
                                      cp  /root/.docker/config.json laravel-docker-lab/
                                """
                             }
                    }
                    container('kaniko') {   
                          sh "rm -rf /kaniko/.docker/config.json"
                          sh 'cp laravel-docker-lab/config.json  /kaniko/.docker/config.json'  
                          sh '''/kaniko/executor  --context "laravel-docker-lab/"  --dockerfile "laravel-docker-lab/Dockerfile" --destination "452226382718.dkr.ecr.us-east-1.amazonaws.com/kimit:$BUILD_ID" '''
                    }
                    
                  
                }
            }     

 // ####################### GitOps  ########################   

            stage('GitOps Update Staging   Helm Chart') {
                steps {
                    container('bash') {
                    sh "apk add git "
                    sh "rm -rf ./*  && git clone https://gitlab.com/devops-course4960906/laravel-docker-lab.git"
                    }

                }  
                post {
                 always {
                     script {
                         // Stage that requires approval
                        input(id: 'stageApproval', message: 'Do you want to proceed with deployment?') 
                    }
                 }
                }
            }    
 // ####################### GitOps  ########################   

            stage('GitOps  Update  Production Helm Chart ') {
                steps {
                    container('bash') {
                    sh "apk add git "
                    sh "rm -rf ./*  && git clone https://gitlab.com/devops-course4960906/laravel-docker-lab.git"
                    }

                }  
            }
// ####################### End of CI Stages  ######################## 



  }
}
