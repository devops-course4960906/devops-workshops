# Use an official Node.js runtime as a parent image
FROM node:16.16 AS build

# Set the working directory to /app
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
#COPY package*.json ./
COPY . .
# Install dependencies
RUN npm install --force
#RUN npm run build 

# Copy the rest of the application code to the working directory
#COPY . .
RUN npm run build
RUN ls -lha
RUN ls -lha ./build/
# Build the production-ready React app
#CMD ["npm", "start"]
# Use an official Nginx image as a parent image
FROM nginx:1.21.1-alpine

# Copy the built React app from the previous stage
COPY --from=build /app/build /usr/share/nginx/html

RUN chmod -R 777 /usr/share/nginx/html
# Copy custom Nginx configuration file if needed
# COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose the Nginx port
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]