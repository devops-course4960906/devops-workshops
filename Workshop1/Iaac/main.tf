# Root module to control all other modules
module "iam" {
  source = "./iam_module"
  # Define input variables for the IAM module
}

module "vpc" {
  source = "./vpc_module"
  # Define input variables for the VPC module
}

module "eks" {
  source = "./eks_module"
  # Define input variables for the EKS module
}

module "s3_cloudfront" {
  source = "./s3_cloudfront_module"
  # Define input variables for the S3 and CloudFront module
  app_name   = "my-laravel-app"
  aws_region = "us-west-2"
}

