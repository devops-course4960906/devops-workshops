variable "app_name" {
  description = "Name of the Laravel app"
  type        = string
}

variable "aws_region" {
  description = "AWS region"
  type        = string
}