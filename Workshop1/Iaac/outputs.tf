output "s3_bucket_name" {
  value = module.s3_cloudfront.s3_bucket_name
}

output "cloudfront_domain_name" {
  value = module.s3_cloudfront.cloudfront_domain_name
}